<?php

class ParseXML {

    private function clean($str)
    {
        $str = strip_tags($str);
        $str = utf8_decode($str);
        $str = str_replace("&nbsp;", "", $str);
        $str = preg_replace('/\s+/', ' ',$str);
        $str = trim($str);
        return $str;
    }

    private function parse_xml($tour)
    {
        $prices = [];
        foreach($tour->DEP as $dep)
        {
            $price = (float) $dep->attributes()->EUR;

            if(isset($dep->attributes()->DISCOUNT))
                $price = (float) $dep->attributes()->EUR * ((100 - (int) trim($dep->attributes()->DISCOUNT, '%')) / 100);

            array_push($prices, number_format($price, 2, '.', ''));
        }
        $response = (string) $tour->Title .'|'. (string) $tour->Code .'|'. (int) $tour->Duration. '|'. (string) $tour->Start.'|'. (string) $tour->End;
        $response .= '|'. $this->clean((string) $tour->Inclusions).'|'. min($prices);

        return $response;
    }

    public function getTours($string)
    {
        $xml = simplexml_load_string($string);


        if(count($xml->children()) > 1)
        {
            $response = [];
            foreach($xml->TOUR as $tour)
            {
                array_push($response, $this->parse_xml($tour));
            }
        }
        else
        {
            $response = $this->parse_xml($xml->TOUR);
        }

        return json_encode(['tours' => $response]);
    }
}

$string = <<<EOD
<?xml version="1.0"?>
<TOURS>
    <TOUR>
        <Title><![CDATA[Anzac1 &amp; Egypt Combo Tour]]></Title>
        <Code>AE-191</Code>
        <Duration>181</Duration>
        <Start>Istanbul1</Start>
        <End>Cairo1</End>
        <Inclusions>
            <![CDATA[<div style="margin: 1px 0px; padding: 1px 0px; border: 0px; outline: 0px; font-size: 14px; vertical-align: baseline; text-align: justify; line-height: 19px; color: rgb(6, 119, 179);">The tour price&nbsp; cover the following services: <b style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-color: transparent;">Accommodation</b>; 5, 4&nbsp;and&nbsp;3 star hotels&nbsp;&nbsp;</div>]]>
        </Inclusions>
        <DEP DepartureCode="AN-17" Starts="04/19/2015" GBP="1458" EUR="1724" USD="2350" DISCOUNT="15%" />
        <DEP DepartureCode="AN-18" Starts="04/22/2015" GBP="1558" EUR="1784" USD="2550" DISCOUNT="20%" />
        <DEP DepartureCode="AN-19" Starts="04/25/2015" GBP="1558" EUR="1784" USD="2550" />
    </TOUR>
    <TOUR>
        <Title><![CDATA[Anzac &amp; Egypt Combo Tour]]></Title>
        <Code>AE-19</Code>
        <Duration>18</Duration>
        <Start>Istanbul</Start>
        <End>Cairo</End>
        <Inclusions>
            <![CDATA[<div style="margin: 1px 0px; padding: 1px 0px; border: 0px; outline: 0px; font-size: 14px; vertical-align: baseline; text-align: justify; line-height: 19px; color: rgb(6, 119, 179);">The tour price&nbsp; cover the following services: <b style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-color: transparent;">Accommodation</b>; 5, 4&nbsp;and&nbsp;3 star hotels&nbsp;&nbsp;</div>]]>
        </Inclusions>
        <DEP DepartureCode="AN-17" Starts="04/19/2015" GBP="1458" EUR="1724" USD="2350" DISCOUNT="15%" />
        <DEP DepartureCode="AN-18" Starts="04/22/2015" GBP="1558" EUR="1784" USD="2550" DISCOUNT="20%" />
        <DEP DepartureCode="AN-19" Starts="04/25/2015" GBP="1558" EUR="1784" USD="2550" />
    </TOUR>
</TOURS>
EOD;

$xml = new ParseXML();
print_r($xml->getTours($string));